import random
import re
class Rnd:
    
    def generaString(self,scelta):
        val = ""
        if(int(scelta) == 0):
            val = random.choice(list(open('./dizionari/nomi.txt')))
        if(int(scelta) == 1):
            val = random.choice(list(open('./dizionari/cognomi.txt')))
        if(int(scelta) == 2):
            val = random.choice(list(open('./dizionari/parole.txt')))

        val = val.strip('\n') 
        val = val.strip('\t')
        val = re.sub(' +', ' ',val)
        return val 
    def generaData(self,annoi,annof):
        mesi31 = [1,3,5,7,8,10,12]
        y = random.randint(int(annoi),int(annof))    
        m = random.randint(1,12)
        
        if(m == 2):
            g = random.randint(1,28)
        if(m in mesi31):
            g = random.randint(1,31)
        else:
            g = random.randint(1,30)

        data= ""
        data+= str(y)

        if(m < 10):
            data +='0'+ str(m)
        else:
            data+=str(m)

        if(g < 10):
            data+= '0'+str(g)
        else:
            data+= str(g)

        return data

    def generaInt(self,inizio,fine):
        return random.randint(inizio,fine)

    def generaAnno(self,annoi, annof):
        return random.randint(annoi,annof)

