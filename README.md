Questo script permette di creare in modo semplice e veloce un gran numero di operazioni di popolazione di tabelle
in mySql tramite il comando <insert into> permettendo di inserire manualmente i dati o crearli in modo casuale, in quest'ultima modalità, dando il nome <cognomi/cognome/nome/nomi> 
ad un campo verranno assegnati nomi e cognomi reali(randomici) altrimenti per un campo varchar senza questo nome sarà associata una stringa di 2 parole casuali.
Oltre alla visualizzazione a schermo del risultato a fine creazione verrà creato un file all'interno della cartella <script> 
con il nome di insert_into_<nometabella>T<data e ora creazione>.sql.

il file .exe permette l'installazione di python portable, una volta installato basterà andare nella cartella di installazione e avviare <WinPython Powershell Prompt> per poi spostarsi nella directory dello script
ed usare il comando <python RSql.py> per avviare lo script.

è necessario che lo script rimanga con le cartelle e i file presenti in questa repository (ad eccezione del file .exe) in quanto sfrutta file presenti nelle varie directory.

Creato da Thomas Andreatta.

