import sys
import os
import signal
import datetime
import shutil
from classi.rnd import Rnd
import re

_version_ = '1.1.1'
_creator_ = 'Thoams Andreatta'
_dateV_ = '27-05-2019'


#region pulisce la console con ctrl+c
def signal_handler(signal, frame):
    os.system('cls' if os.name=='nt' else 'clear')
    printLogo(1) 
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def cls(): 
    os.system('cls' if os.name=='nt' else 'clear')
    printLogo(0)
#endregion

type = ['int','varchar','date','year']

def printLogo(num):
    if(int(num) == 0):
        print("---RapidSQL---")
    else:
        print("---RapidSQL INTERROTTO---")

def tabella():
    nome = input("[+] nome tabella> ")
    conferma = False
    while(conferma == False):
        tmp = input("[+] confermare :"+nome+"[enter/n]")
        if(tmp.lower() == ""):
            conferma = True
        elif(tmp.lower() == "n"):            
            tmp = input("[+] nome tabella>")
       
    
    return nome

def numero():
    numeroc = input("[+] numero campi> ")
    nomicampi = []
    conferma = False
    while(conferma == False):
        if(numeroc.isdigit()):
            tmp = input("[+] confermare campi:"+numeroc+"[enter/n]")
            if(tmp.lower() == "" and numeroc.isdigit()):
                conferma = True
            elif(tmp.lower() == "n" and numeroc.isdigit()):            
                numeroc = input("[+] numero campi>")        
        elif(numeroc.isdigit() == False):
            numeroc = input("[+] numero campi> ")

    cls()
    for x in range(0,int(numeroc)):
        nomicampi.append(input('[+] inserire il nome del '+str(x+1)+'* campo> '))

    
    
    approvato = False
    while(approvato == False):
        cls()
        print('[+] Assicurarsi che i nomi dei campi siano corretti(charSensitive).\nPremere "invio" per confermare,' \
            'altrimenti selezionare il nome del campo da modificare \n')

        i = 0
        for x in nomicampi:        
            print('['+str(i)+'] '+x)
            i = i+1

        tmp = input('[+] conferma/selezione campo> ')
        if(tmp.isdigit()):
            if(int(tmp) <= int(len(nomicampi)-1)):                
                newname = input('[+] inserire il nuovo nome per il campo <'+nomicampi[int(tmp)]+'> >')
                nomicampi[int(tmp)] = newname
        elif(tmp == ""):
            approvato = True
        else:
            print('inserire un valore <= a '+str( len(nomicampi)))

    cls()
    for x in range(0, int(len(nomicampi))):
        nomicampi[x] = nomicampi[x].strip(' ')
        nomicampi[x] = nomicampi[x].strip('\t')
    return nomicampi

def tipo(numero,nomi):
    
    valori = []
    print('[+] int         [+] varchar')
    print('[+] date        [+] year')
    print('\n')
    for i in range(0,numero):
        tipotmp = input("[+] tipo campo <"+nomi[i]+"> > ")
        conferma = False                    
        while(conferma == False):            
            tmp = input("[+] confermare il tipo: "+tipotmp+" per il campo <"+nomi[i]+"> [enter/n]> ")
            if(tmp.lower() == "" and tipotmp.lower() in type):
                conferma = True
            elif(tmp.lower() == "n" and tipotmp.lower() in type):            
                tipotmp = input("[+] tipo campo <"+nomi[i]+"> > ") 
            else:
                tipotmp = input("[+] inserire un tipo valido > ")
        valori.append(tipotmp.lower())

    return valori

def raccoltaValori(tipo, nome):    
    cls()
    valori = []
    legittimo = False
    while(legittimo == False):
                    value = input('[+] inserire il numero di tuple> ')
                    if(value.isdigit()):
                        legittimo = True
                        numerorighe = value

    for i in range(0,int(numerorighe)):
        cls()
        print('------TUPLA '+str(i+1)+'------')
        for x in tipo:
            value = ""
            accettato = False
            if(x == 'int'):
                while(accettato == False):
                    value = input('[+] inserire un valore ('+x+') per il campo> ')
                    if(value.isdigit()):
                        accettato = True
            elif('char' in x):
                while(accettato == False):
                    value = input('[+] inserire un valore ('+x+') per il campo> ')                
                    accettato = True                
            elif(x == 'date'):
                while(accettato == False):
                    value = input('[+] inserire un valore ('+x+') per il campo> ')
                    if(value.isdigit() and len(value) == 8):                    
                        accettato = True        
            elif(x == 'year'):
                while(accettato == False):
                    value = input('[+] inserire un valore ('+x+') per il campo> ')
                    if(value.isdigit() and (len(value) == 2 or len(value) == 4)):                    
                        accettato = True   

            valori.append(value)
    return valori

def componiINTO(nomi,tabella, valori):
    
    file1 = open('tmp_insert.txt',"a")
    
    numerovalori = len(valori)
    numeronomi = len(nomi)
    giri = int (numerovalori/numeronomi)
    
    for i in range(0, giri):
        stringa = 'insert into '+ tabella+'('

        for nome in nomi:
            stringa += nome+','
        
        stringa = list(stringa)
        stringa[int(len(stringa)) -1] = ')'
        stringa = "".join(stringa)
        stringa += ' values('

        for j in range(0,numeronomi):
            
            if(valori[0].isdigit()):
                stringa += valori[0]+','                
            else:
                stringa += '"'+valori[0]+'",'
            del valori[0]
            
        stringa = list(stringa)
        stringa[int(len(stringa)) -1] = ');'
        stringa = "".join(stringa)    
        file1.write(stringa+'\n')
    file1.close()

def autoINTO(nomi,tabella,tipi):
    
    cls()
    
    
    file1 = open('tmp_insert.txt',"a")

    numerorighe = 0
    legittimo = False
    while(legittimo == False):
                    value = input('[+] inserire il numero di tuple> ')
                    if(value.isdigit()):
                        legittimo = True
                        numerorighe = value

    print("---Creazione script---")
    numeronomi = len(nomi)
    
    
    for i in range(0,int( numerorighe)):
        stringa = 'insert into '+ tabella+'('
        r = Rnd()
        for nome in nomi:
            stringa += nome+','
        
        stringa = list(stringa)
        stringa[int(len(stringa)) -1] = ')'
        stringa = "".join(stringa)
        stringa += ' values('

        for j in range(0,numeronomi):   
            tipo = tipi[j]
            nomec = nomi[j]         
            if(tipo == 'int'):
               stringa+=str( r.generaInt(1,10000)  )

            elif(tipo == 'varchar'):
                if(nomec.lower() == 'nome' or nomec.lower() == 'nomi' or nomec.lower() == 'name' or nomec.lower() == 'names'):
                    stringa+='"'+str( r.generaString(0))+'"'
                elif(nomec.lower() == 'cognome' or nomec.lower() == 'cognomi' or nomec.lower() == 'surname'):
                    stringa+='"'+str( r.generaString(1))+'"'
                else:
                    stringa+='"'+str( r.generaString(2))+'"'

            elif(tipo == 'date'):
                stringa+='"'+str( r.generaData(1990,2019))+'"'

            elif(tipo == 'year'):
                stringa+=str( r.generaAnno(1990,2019))           
            stringa += ','
            
        stringa = list(stringa)
        stringa[int(len(stringa)) -1] = ');'
        stringa = "".join(stringa)  
        stringa = stringa.strip('\n') 
        stringa = stringa.strip('\t')
        stringa = re.sub(' +', ' ',stringa) 
        file1.write(stringa+'\n')
    file1.close()

def main():
    nomeTabella = tabella()
    nomicampi = numero()
    tipovalori = tipo(int(len(nomicampi)),nomicampi)
    
    
    """tipovalori = ['int','varchar','date']
    nomicampi= ['int','char','data']
    valori = ['34','stringa','20000619','35','stringa2','80008080']
    nomeTabella = 'tabella' """
    accettato = False
    cls()
    while(accettato == False):
        scelta = input('\n[1] inserimento manuale         [2] inserimento automatico\n')
        if(int(scelta) == 1 ):
            componiINTO(nomicampi,nomeTabella,valori)
            valori = raccoltaValori(tipovalori,nomicampi)
            accettato = True
        elif(int(scelta) == 2):
            autoINTO(nomicampi,nomeTabella,tipovalori) 
            accettato = True

    return nomeTabella

if __name__ == "__main__":
    cls()   
    nome = main()
    cls()
    print('------- SCRIPT -------\n')
    with open('tmp_insert.txt', 'r') as fin:
        print (fin.read())
    print('\n----------------------')

    tmp = str(datetime.datetime.now())
    tmp = list(tmp)    
    ora = ""
    for x in tmp:
        if(int(len(ora)) < 16):
            if(x == ':'):
                ora += '_'
            else:
                ora += x

    ora.replace(':','_')
    os.rename("tmp_insert.txt","insert_into_"+nome+'_T'+ora+".sql")
    path =os.getcwd()+'/script/'+ "insert_into_"+nome+'_T'+ora+".sql" 
    if(os.path.isdir('./script') == False):
        os.makedirs('script')

    shutil.move(os.getcwd()+'/'+ "insert_into_"+nome+'_T'+ora+".sql", path)

    print('script creato e salvato in <'+path+'>')
    